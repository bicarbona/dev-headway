<?php

/**

 Modifikovanie ACF color pickeru
 http://automattic.github.io/Iris/
**/

function load_javascript_on_admin_edit_post_page() {
  global $parent_file;

  // If we're on the edit post page.
 // if ($parent_file == 'post-new.php' || $parent_file == 'edit.php') {
    echo "
      <script>
      jQuery(document).ready(function(){
        jQuery('.color_picker').iris({
 		width: 800,
          palettes: ['#125', '#459', '#78b', '#ab0', '#de3', '#f0f','#125', '#459', '#78b', '#ab0', '#de3', '#f0f'],

          change: function(event, ui){
            jQuery(this).parents('.wp-picker-container').find('.wp-color-result').css('background-color', ui.color.toString());
          }
        });
      });
      </script>
    ";
//  }
}
add_action('in_admin_footer', 'load_javascript_on_admin_edit_post_page');

/**

 Modifikovanie ACF color pickeru

**/

/*Define Comment Widget*/
register_sidebars(1,
    array(
        'name' => 'MY Widget',
        'before_widget' => '<li class="widget %2$s" id="%1$s">',
        'after_widget' => '</li>',
        'before_title' => '<span class="widget-title">',
        'after_title' => '</span>'
    )
);
/**

 LESS variable ACF
 
**/







// if (class_exists('WPLessPlugin')){
//     $less = WPLessPlugin::getInstance();

//     //$less->addVariable('myColor', '#666');
//     // you can now use @myColor in your *.less files

// if (function_exists(get_field)){

// 	$orange = get_field('less_orange','option');
// $yellow = get_field('less_yellow','option');
// $black = get_field('less_black','option');

// // $green = '#fff';
//     $less->setVariables(array(
//         'orange' => $orange,
//         'yellow' => $yellow,
//          'black' => $black,
//     ));
//     // you can now use @minSize in your *.less files
//     // @myColor value has been updated to #777
// }

// }

// you can also use .less files as mce editor style sheets
//add_editor_style( 'editor-style.less' );


/**

 LESS Enque
 
**/


add_action('init', 'theme_enqueue_styles');

function theme_enqueue_styles() {
	if ( ! is_admin() )
	wp_enqueue_style( 'less-style', get_stylesheet_directory_uri() . '/less/main.less' );
  //  wp_enqueue_style('theme-extra', get_stylesheet_directory_uri().'/stylesheets/theme-extra.less');
}



/**
:: Auto-Includer
 
Loads PHP files from the /includes/ directory.  For WordPress sites,
this code would go in your functions.php file.
 
**/
 
foreach (glob(__DIR__ . '/includes/*.php') as $my_theme_filename) {
 
  // Exclude files whose names contain -sample
  if (!strpos($my_theme_filename, '-sample') ) {
      include_once $my_theme_filename;
  }
 
}


?>