<?php




if (class_exists('WPLessPlugin')){
    $less = WPLessPlugin::getInstance();

    //$less->addVariable('myColor', '#666');
    // you can now use @myColor in your *.less files

$orange = get_field('less_orange','option');
$yellow = get_field('less_yellow','option');
$black = get_field('less_black','option');

// $green = '#fff';
    $less->setVariables(array(
        'orange' => $orange,
        'yellow' => $yellow,
         'black' => $black,
    ));
    // you can now use @minSize in your *.less files
    // @myColor value has been updated to #777
}

// you can also use .less files as mce editor style sheets
//add_editor_style( 'editor-style.less' );




add_action('init', 'theme_enqueue_styles');

function theme_enqueue_styles() {
	if ( ! is_admin() )
wp_enqueue_style( 'less-style', get_stylesheet_directory_uri() . '/less/main.less' );
  //  wp_enqueue_style('theme-extra', get_stylesheet_directory_uri().'/stylesheets/theme-extra.less');
}



/* --------------------------------------------------------------------
:: Auto-Includer
 
Loads PHP files from the /includes/ directory.  For WordPress sites,
this code would go in your functions.php file.
 
-------------------------------------------------------------------- */
 
foreach (glob(__DIR__ . '/includes/*.php') as $my_theme_filename) {
 
  // Exclude files whose names contain -sample
  if (!strpos($my_theme_filename, '-sample') ) {
      include_once $my_theme_filename;
  }
 
}



// Headway Specific Code
 
// Removes a few things from Headway when the child theme is activated.
// In this case, the design editor is turned off, and there's not outputted css from Headway,
// other than what's needed for the grid
add_action('headway_setup_child_theme', 'aj_child_setup');
function aj_child_setup() {
	//remove_theme_support('headway-design-editor');
	remove_theme_support('headway-structure-css');
}
 
// Register Custom Block Classes
add_action('init', 'aj_child_theme_add_block_styles');
function aj_child_theme_add_block_styles() {
 
	HeadwayChildThemeAPI::register_block_style(array(
		'id' => 'green',
		'name' => 'Groovy Green',
		'class' => 'green'
		// This will show for every block
	));
 
	HeadwayChildThemeAPI::register_block_style(array(
		'id' => 'orange',
		'name' => 'Orangey Orange',
		'class' => 'orange'
	));
 
	HeadwayChildThemeAPI::register_block_style(array(
		'id' => 'yellow',
		'name' => 'Banana Yellow',
		'class' => 'yellow'
	));
 
	HeadwayChildThemeAPI::register_block_style(array(
		'id' => 'blue',
		'name' => 'Badass Blue',
		'class' => 'blue'
		//'block-types' => array('navigation', 'footer')
		// This will only show for certain blocks that are in the array.
	));
 
}
 
// Loads the Panel functions file created in the Child Theme
add_action('headway_visual_editor_init', 'aj_load_panel_options');
function aj_load_panel_options() {
	require_once( get_stylesheet_directory(). '/aj-hw-options.php' );
}
 
// Adds an inline style to the <head> so that you can
add_action('wp_head', 'switch_background_image');
function switch_background_image() {
	$image = HeadwayOption::get('aj-background-image', 'general');
	
	switch ($image)
	{
	case 'square_bg.png':
		break;
	case 'argyle.png':
		echo '<style type="text/css"> body { background: url(\'wp-content/themes/aj-morris/images/argyle.png\'); } </style>';
		break;
	case 'robots.png':
		echo '<style type="text/css"> body { background: url(\'wp-content/themes/aj-morris/images/robots.png\'); } </style>';
		break;
	case 'pinstriped_suit.png':
		echo '<style type="text/css"> body { background: url(\'wp-content/themes/aj-morris/images/pinstriped_suit.png\'); } </style>';
		break;
	}
}


?>